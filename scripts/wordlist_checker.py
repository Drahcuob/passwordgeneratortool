#!/usr/bin/python3

"""
Check wordlist encoding
"""

import sys
import argparse

def main(file_path, output_path):
    counter = 0
    with open(output_path, 'w') as ofile:
        with open(file_path, 'rb') as ifile:
            for line in ifile:
                line = line[:-1]
                try:
                    line = line.decode('utf-8')
                    ofile.write(line)
                    ofile.write('\n')
                except:
                    ...
                finally:
                    counter += 1
                    if counter % 500 == 0:
                        print(counter)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Remove non UTF-8 passwords from wordlist output result on stdout")
    parser.add_argument('-p', '--path', required=True, type=str, help='wordlist path')
    parser.add_argument('-o', '--output', required=True, type=str, help='output path')
    args = parser.parse_args()
    main(args.path, args.output)
