# PasswordGeneratorTool

## Description

Password Testing Tool.

## Installation

There are two different way to install our tool. If your using Windows or MacOS please follow the first installation method, as we do not test our tool on those operating systems. If you're using Linux you can follow any of the two methods. However, we also recommend the use of the first technique.

### 1. Docker

First install docker (https://www.docker.com/). Then run the following commands.

```
$> docker build -t passwordgeneratortool .
$> docker run -d -p 3000:3000 -p 7890:7890 --name passwordgeneratortool passwordgeneratortool ./rundocker.sh

# To stop the container run
$> docker stop passwordgeneratortool
```

### 2. Without Docker

You need to install Python3, Pip3, NodeJS, NPM. Then, download this repository and install the dependencies by running the following commands.
```
$ git clone git@gitlab.com:Drahcuob/passwordgeneratortool.git
$ cd passwordgeneratortool
$ pip3 install -r api/requirements.txt
$ cd front
$ npm install
```

#### Drupal PSM

Install php module to launch drupal PSM module

```
$ cd ./api/modules/drupal/
$ composer install
```

### Running and Connecting to a shell on the docker container

If the container is running on background and you need to connect using a shell afterward you can use this command:
```
$> docker exec -it passwordgeneratortool /bin/bash
```

If you just wish to connect when nothing is running use this command:
```
$> docker run -it -p 3000:3000 -p 7890:7890 passwordgeneratortool
```

## Contributing

You can contribute by adding new Password Strength Meter [here](https://gitlab.com/Drahcuob/passwordgeneratortool/tree/master/api/modules)

## License

This software is available under the following licenses:
- MIT

## Citation

- how can this projet be cited
- provide doi

## Contact

- Jean Plancher
Master student, Department of Computer Science, University of Kent, UK
Email: jbcp2@kent.ac.uk

- Cyrvan Bouchard
Master student, Department of Computer Science, University of Kent, UK
Email: cdab2@kent.ac.uk
