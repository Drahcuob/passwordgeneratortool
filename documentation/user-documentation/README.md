# User Documentation

## Compilation

HTML document was translated form latex with `pandoc`. The command ran was `$ pandoc -o userdoc.html userdoc.tex`.

Note: During the translation pandoc does not set chartset on HTML file. You need to manually add the header as follow
```
<head>
	<meta chartset="utf-8" />
</head>
```
