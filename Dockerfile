FROM ubuntu

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /usr/src/app

# copy requirements files

ADD . .

RUN apt-get update

# install python

RUN apt-get install -y python3 python3-pip python python-pwquality

# install python dependencies

RUN apt-get install -y libenchant1c2a
WORKDIR /usr/src/app/api
RUN pip3 install --no-cache-dir -r requirements.txt

# install php, composer and dependencies

WORKDIR /usr/src/app/api/modules/drupal
RUN apt-get install -y php php-zip curl
RUN curl -s https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN composer install

# install node, npm and dependencies

WORKDIR /usr/src/app/front
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN npm install


# Make port 3000 and 7890 available to the world outside this container

EXPOSE 7890
EXPOSE 3000

# Setup working directory

WORKDIR /usr/src/app
