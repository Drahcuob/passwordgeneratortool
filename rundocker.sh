#!/bin/bash

cd ./api/
./main.sh &>> back_log.txt &
cd ../front/
npm start &>> front_log.txt &

wait
