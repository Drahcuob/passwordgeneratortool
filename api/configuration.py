import logging

host = '0.0.0.0'
port = 7890
max_password_rate = 500
wordlist_directory = './wordlists/'

logfile_name = 'api_logfile'
logfile_max_byte = 2000
logfile_backup_count = 0
logfile_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logfile_level = logging.DEBUG
