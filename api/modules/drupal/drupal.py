from modules.types.module_types import module_types
import os

name = 'Drupal'
function_name = 'drupal'
description = """
Drupal: Drupal password checker is based on the php version of zxcvbn.
"""
module_type = module_types.password_strength_meter

import json
import subprocess

def check(pwds):
    dirpath = os.getcwd()
    args = ["/usr/bin/php", dirpath + "/modules/drupal/drupal.php"] + pwds
    output = subprocess.check_output(args)
    return json.loads(output.decode('utf-8'))

def drupal(pwds):
    if type(pwds) == type([]):
        return check(pwds)
    elif type(pwds) == type(''):
        return check([pwds])
    else:
        return None
    
