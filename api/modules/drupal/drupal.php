<?php
    require_once 'vendor/autoload.php';

    use ZxcvbnPhp\Zxcvbn;

    function check($drupal, $pwd) {
        $result = $drupal->passwordStrength($pwd);
        return $result['score'] / 4.0;
    }

    function checks($drupal, $pwds) {
        $scores = array();
        foreach ($pwds as $pwd) {
            $score = array();
            $score["password"] = $pwd;
            $score["result"] = check($drupal, $pwd) * 100;

            array_push($scores, $score);
        }
        return $scores;
    }

    if (isset($argv)) {
        array_shift($argv);
        $drupal = new ZxcvbnPhp\Zxcvbn;
        $results = checks($drupal, $argv);
        echo json_encode($results)."\n";
    }
?>
