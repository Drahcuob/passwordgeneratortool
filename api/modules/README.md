# How to add new Password Strength Meters (or Password Rule)

## Description

One of this project aims is to compare and draw conclusion about PSM.
So by adding new PSM the project will be improved.

## Contributing

In order to add your module you'll need to create a new directory with a python script inside.
This script we'll be the connection between your module and our server. 

We define a module by three mandatory things:
- its name
- the name of the function
- a description
- its type (psm or password rule)

You'll need to add those four variables to the python script
```python
from modules.types.module_types import module_types

name = 'my_PSM' # name displayed on the front
function_name = 'my_function' # name of the function the server will call
description = """
my_PSM: password meter used by https://my_PSM.net/ 
module_type = module_types.password_strength_meter
""" # displayed when selected on the front 
module_type = module_types.password_strength_meter
# or
module_type = module_types.password_rule
# depending on what are your module
```

The project actually support PSM written both in python (2 and 3) and nodejs.
Your script will receive one password or a list of passwords and need to return a array of dictionary following this schema:
```json
{ 'password' : passwd, 'result': score }
```

### python3:

Since our server is run by python3 you can juste import your PSM.

```python
import myPSM

def my_function(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result' : myPSM(password) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                   'result': myPSM(passwords) }
        ]
    else:
        return None
```

### python2:

If your PSM was written in python2 you'll need to use call_python_version, which is a proxy on the execnet python library.

 ```python
 from modules.utils.pythonproxy import call_python_version

 def hsimp(passwords):
    py2code = """
    import myPSM
    func = myPSM.check
    args = %s # need the '%s' to receive the passwords
    results = []
    for arg in args:
        results.append({ 'password': arg,
                         'result': func(arg) })
    %s(results) # need the '%s' to return the passwords
    """
    if type(passwords) == type([]):
        return call_python_version("2.7", py2code, passwords)
    elif type(passwords) == type(''):
        return call_python_version("2.7", py2code, [passwords])
    else:
        return None
 ```

### nodejs:

If your PSM was written in nodejs you'll need to use execute, which is a proxy on a subprocess running a nodejs shell.

```python
from modules.utils.nodeproxy import execute

def my_function(passwords):
    jscode = '''function (args) {
    var myPSM = require('./modules/myPSM/myPSM.js');
    var scores = [];
    for (i = 0; i < args.length; i++) {
    var tmp = {'password': args[i], 'result': myPSM(args[i])};
    scores.push(tmp);
    }
    return scores;
    }
    '''

    if type(passwords) == type([]):
        return execute(jscode, args=[passwords])
    elif type(passwords) == type(''):
        return execute(jscode, args=[[passwords]])
    else:
        return None
```

## Example

You can see examples inside `/modules/example/example.py`