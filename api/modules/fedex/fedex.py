from modules.types.module_types import module_types
from modules.utils.nodeproxy import execute

name = 'Fedex'
function_name = 'fedex'
description = """
Fedex: password meter used by fedex when authenticating
"""
module_type = module_types.password_strength_meter

def fedex(passwords):
    jscode = '''function (args) {
    var fedex = require('./modules/fedex/fedex.js');
    var scores = [];
    for (i = 0; i < args.length; i++) {
    res = fedex(args[i])
    if (res === 0 || res === 1) { res = 0 }
    else if (res === 2 || res === 3) { res = 33 }
    else if (res === 4) { res = 66 }
    else { res = 100 }
    var tmp = {'password': args[i], 'result': res};
    scores.push(tmp);
    }
    return scores;
    }
    '''

    if type(passwords) == type([]):
        return execute(jscode, args=[passwords])
    elif type(passwords) == type(''):
        return execute(jscode, args=[[passwords]])
    else:
        return None
        
