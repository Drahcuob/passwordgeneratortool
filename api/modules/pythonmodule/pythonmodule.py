from modules.types.module_types import module_types
import passwordmeter

name = 'Pythonmodule'
function_name = 'pythonmodule'
description = """
Pythonmodule: a configurable, extensible password strength measuring library in python.
Source: https://github.com/cadithealth/passwordmeter
"""
module_type = module_types.password_strength_meter

def pythonmodule(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result': int(passwordmeter.test(password)[0] * 100) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                   'result': int(passwordmeter.test(passwords)[0] * 100) }
        ]
    else:
        return None
