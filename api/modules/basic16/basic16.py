from modules.types.module_types import module_types

name = 'Basic16'
function_name = 'basicfunc'
description = """
Basic16: The password must have a length of 16 characters
"""
module_type = module_types.password_rule

def check(pwd):
    size = len(pwd)
    if size >= 16:
        return 1 * 100
    else:
        return (size / 16) * 100

def basicfunc(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result': check(password) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                   'result': check(passwords) }
        ]
    else:
        return None
