from modules.types.module_types import module_types
from modules.utils.nodeproxy import execute

name = 'Npmmodule'
function_name = 'npmmodule'
description = """
Npmmodule: a password strength meter for jQuery.
Source: https://github.com/elboletaire/password-strength-meter
"""
module_type = module_types.password_strength_meter

def npmmodule(passwords):
    jscode = '''function (args) {
    var npm = require('./modules/npmmodule/npmmodule.js');
    var scores = [];
    for (i = 0; i < args.length; i++) {
    scores.push({'password': args[i], 'result': npm.calculateScore(args[i])});
    }
    return scores;
    }
    '''

    if type(passwords) == type([]):
        return execute(jscode, args=[passwords])
    elif type(passwords) == type(''):
        return execute(jscode, args=[[passwords]])
    else:
        return None
