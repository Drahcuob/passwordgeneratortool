from modules.types.module_types import module_types

name = 'Basic8'
function_name = 'basicfunc'
description = """
Basic8: The password must have a length of 8 characters
"""
module_type = module_types.password_rule

def check(pwd):
    size = len(pwd)
    if size >= 8:
        return 1 * 100
    else:
        return (size / 8) * 100

def basicfunc(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result': check(password) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                   'result': check(passwords) }
        ]
    else:
        return None
