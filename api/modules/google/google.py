from modules.types.module_types import module_types

name = 'Google'
function_name = 'google'
description = """
Google: Server-side password strength meters. During the test does POST request to `https://accounts.google.com/RatePassword`.
Do not use with big wordlist.
"""
module_type = module_types.password_strength_meter

import requests as rq

def check(password):
    data = {
        'Passwd': password,
    }
    resp = rq.post('https://accounts.google.com/RatePassword', data=data)
    if resp.status_code == 200:
        return (int(resp.text)-1) * 33.33
    else:
        return 0


def google(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result': check(password) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                   'result': check(passwords) }
        ]
    else:
        return None

