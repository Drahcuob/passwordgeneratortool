from modules.types.module_types import module_types

name = 'Ebay'
function_name = 'ebay'
description = """
Ebay: Server-side password strength meters. During the test does POST request to `reg.ebay.fr/reg/ajax`.
Do not use with big wordlist. 
"""
module_type = module_types.password_strength_meter

import requests as rq

def check(password):
    data = {
        'PASSWORD': password,
        'email': 'user@gmail.com',
        'mode': 25
    }
    resp = rq.post('https://reg.ebay.fr/reg/ajax', data=data)
    if resp.status_code == 200:
        result = resp.json()['matchedPwdRules']
        return (len(result) / 3) * 100
    else:
        return 0 

def ebay(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result': check(password) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                   'result': check(passwords) }
        ]
    else:
        return None

