from modules.types.module_types import module_types
from modules.utils.nodeproxy import execute

name = 'Twitter'
function_name = 'twitter'
description = """
Twitter: password meter used by twitter when authenticating
"""
module_type = module_types.password_strength_meter

def twitter(passwords):
    jscode = '''function (args) {
    var twitter = require('./modules/twitter/twitter_psm.js');
    var scores = [];
    for (i = 0; i < args.length; i++) {
    result = twitter(args[i])
    if (result < 0) { result = 0 }
    var tmp = {'password': args[i], 'result': result};
    scores.push(tmp);
    }
    return scores;
    }
    '''

    if type(passwords) == type([]):
        return execute(jscode, args=[passwords])
    elif type(passwords) == type(''):
        return execute(jscode, args=[[passwords]])
    else:
        return None
