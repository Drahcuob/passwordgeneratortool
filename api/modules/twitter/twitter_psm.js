attr = {};
attr.bannedPasswords = [ "000000", "111111", "11111111", "112233", "121212", "123123", "123456", "1234567", "12345678", "123456789", "131313", "232323", "654321", "666666", "696969", "777777", "7777777", "8675309", "987654", "aaaaaa", "abc123", "abc123", "abcdef", "abgrtyu", "access", "access14", "action", "albert", "alberto", "alexis", "alejandra", "alejandro", "amanda", "amateur", "america", "andrea", "andrew", "angela", "angels", "animal", "anthony", "apollo", "apples", "arsenal", "arthur", "asdfgh", "asdfgh", "ashley", "asshole", "august", "austin", "badboy", "bailey", "banana", "barney", "baseball", "batman", "beatriz", "beaver", "beavis", "bigcock", "bigdaddy", "bigdick", "bigdog", "bigtits", "birdie", "bitches", "biteme", "blazer", "blonde", "blondes", "blowjob", "blowme", "bond007", "bonita", "bonnie", "booboo", "booger", "boomer", "boston", "brandon", "brandy", "braves", "brazil", "bronco", "broncos", "bulldog", "buster", "butter", "butthead", "calvin", "camaro", "cameron", "canada", "captain", "carlos", "carter", "casper", "charles", "charlie", "cheese", "chelsea", "chester", "chicago", "chicken", "cocacola", "coffee", "college", "compaq", "computer", "consumer", "cookie", "cooper", "corvette", "cowboy", "cowboys", "crystal", "cumming", "cumshot", "dakota", "dallas", "daniel", "danielle", "debbie", "dennis", "diablo", "diamond", "doctor", "doggie", "dolphin", "dolphins", "donald", "dragon", "dreams", "driver", "eagle1", "eagles", "edward", "einstein", "erotic", "estrella", "extreme", "falcon", "fender", "ferrari", "firebird", "fishing", "florida", "flower", "flyers", "football", "forever", "freddy", "freedom", "fucked", "fucker", "fucking", "fuckme", "fuckyou", "gandalf", "gateway", "gators", "gemini", "george", "giants", "ginger", "gizmodo", "golden", "golfer", "gordon", "gregory", "guitar", "gunner", "hammer", "hannah", "hardcore", "harley", "heather", "helpme", "hentai", "hockey", "hooters", "horney", "hotdog", "hunter", "hunting", "iceman", "iloveyou", "internet", "iwantu", "jackie", "jackson", "jaguar", "jasmine", "jasper", "jennifer", "jeremy", "jessica", "johnny", "johnson", "jordan", "joseph", "joshua", "junior", "justin", "killer", "knight", "ladies", "lakers", "lauren", "leather", "legend", "letmein", "letmein", "little", "london", "lovers", "maddog", "madison", "maggie", "magnum", "marine", "mariposa", "marlboro", "martin", "marvin", "master", "matrix", "matthew", "maverick", "maxwell", "melissa", "member", "mercedes", "merlin", "michael", "michelle", "mickey", "midnight", "miller", "mistress", "monica", "monkey", "monkey", "monster", "morgan", "mother", "mountain", "muffin", "murphy", "mustang", "naked", "nascar", "nathan", "naughty", "ncc1701", "newyork", "nicholas", "nicole", "nipple", "nipples", "oliver", "orange", "packers", "panther", "panties", "parker", "password", "password", "password1", "password12", "password123", "patrick", "peaches", "peanut", "pepper", "phantom", "phoenix", "player", "please", "pookie", "porsche", "prince", "princess", "private", "purple", "pussies", "qazwsx", "qwerty", "qwertyui", "rabbit", "rachel", "racing", "raiders", "rainbow", "ranger", "rangers", "rebecca", "redskins", "redsox", "redwings", "richard", "robert", "roberto", "rocket", "rosebud", "runner", "rush2112", "russia", "samantha", "sammy", "samson", "sandra", "saturn", "scooby", "scooter", "scorpio", "scorpion", "sebastian", "secret", "sexsex", "shadow", "shannon", "shaved", "sierra", "silver", "skippy", "slayer", "smokey", "snoopy", "soccer", "sophie", "spanky", "sparky", "spider", "squirt", "srinivas", "startrek", "starwars", "steelers", "steven", "sticky", "stupid", "success", "suckit", "summer", "sunshine", "superman", "surfer", "swimming", "sydney", "tequiero", "taylor", "tennis", "teresa", "tester", "testing", "theman", "thomas", "thunder", "thx1138", "tiffany", "tigers", "tigger", "tomcat", "topgun", "toyota", "travis", "trouble", "trustno1", "tucker", "turtle", "twitter", "united", "vagina", "victor", "victoria", "viking", "voodoo", "voyager", "walter", "warrior", "welcome", "whatever", "william", "willie", "wilson", "winner", "winston", "winter", "wizard", "xavier", "xxxxxx", "xxxxxxxx", "yamaha", "yankee", "yankees", "yellow", "zxcvbn", "zxcvbnm", "zzzzzz"]
attr.minLength = 6;

undoBannedPasswordROT13 = function() {
    var e = []
    for (t = 0, s = attr.bannedPasswords.length; t < s; t++)
	e.push(this.attr.bannedPasswords[t].replace(/[a-z]/gi, function(e) {
	    var t = e.charCodeAt(0), s = t + 13;
	    return (t <= 90 && s > 90 || s > 122) && (s -= 26), String.fromCharCode(s)
	}));
    attr.bannedPasswords = e
}

module.exports = function(passwd) {
    function i(passwd, t) {
	return t && passwd.toLowerCase() === ("" + t).toLowerCase()}
    function n() {
	return 0
    }
    function a(e, passwd) {
	for(var s = "", i = 0; i < passwd.length; i++) {
	    var o = !0, n = void 0;
	    for(n = 0 ; n < e && n + i + e < passwd.length; n++) o = o && passwd.charAt(n + i) === passwd.charAt(n + i + e); n < e && (o = !1), o ? (i += e - 1, o = !1) : s += passwd.charAt(i)
	}
	return s
    }
    
    var r = 0;
    if (passwd.length < attr.minLength)
	return passwd.length;
    if (attr.bannedPasswords.indexOf(passwd.toLowerCase()) > -1)
	return n();
    if (attr.requireStrong) {
	var c = "# ` ~ ! @ $ % ^ & * ( ) - _ = + [ ] { } \\ | ; : ' \" , . < > / ?".split(" ");
	c = c.map(function(e) {
	    return"\\" + passwd
	}).join("");
	var l = ["\\d", "[a-z]", "[A-Z]", "[" + c + "]"], u = l.map(function(e) {
	    return"(?=.*" + e + ")"
	}).join("");
	if (!passwd.match(new RegExp("(" + u + "){10,}")))
	    return 0
    }

    r += 4 * passwd.length
    r += 1 * (a(1, passwd).length - passwd.length)
    r += 1 * (a(2, passwd).length - passwd.length)
    r += 1 * (a(3, passwd).length - passwd.length)
    r += 1 * (a(4, passwd).length - passwd.length)
    if (passwd.match(/(.*[0-9].*[0-9].*[0-9])/))
	r += 5
    if (passwd.match(/(.*[!@#$%^&*?_~].*[!@#$%^&*?_~])/))
	r += 5
    if (passwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
	r += 10
    if (passwd.match(/([a-zA-Z])/) && passwd.match(/([0-9])/))
	r += 15
    if (passwd.match(/([!@#$%^&*?_~])/) && passwd.match(/([0-9])/))
	r += 15
    if (passwd.match(/([!@#$%^&*?_~])/) && passwd.match(/([a-zA-Z])/))
	r += 15
    if (passwd.match(/^\w+$/) || passwd.match(/^\d+$/))
	r -= 10
    if (r < 0)
	r = 0
    if (r > 100)
	r = 100

    return r

    //  < 34 ? {
    // 	score : r,
    // 	message : Object(o.default)("Faible"),
    // 	reason : "weak"
    // } : r < 50 ? {
    // 	score : r,
    // 	message : Object(o.default)("Bon"),
    // 	reason : "good"
    // } : r < 75 ? {
    // 	score : r,
    // 	message : Object(o.default)("Fort"),
    // 	reason : "strong"
    // } : {
    // 	score : r,
    // 	message : Object(o.default)("Très fort"),
    // 	reason : "verystrong"
    // }
}

