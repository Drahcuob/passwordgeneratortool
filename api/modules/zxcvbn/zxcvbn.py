from modules.types.module_types import module_types
from modules.utils.nodeproxy import execute

name = 'Dropbox'
function_name = 'zxcvbn'
description = """
zxcvbn: password meter used by https://www.dropbox.com/fr/ when authenticating
Source: https://github.com/dropbox/zxcvbn
"""
module_type = module_types.password_strength_meter

def zxcvbn(passwords):
    jscode = '''function (args) {
    var zxcvbn = require('./modules/zxcvbn/zxcvbn.js');
    var scores = [];
    for (i = 0; i < args.length; i++) {
    result = zxcvbn(args[i]).score * 25
    scores.push({'password': args[i], 'result': result});
    }
    return scores;
    }
    '''

    if type(passwords) == type([]):
        return execute(jscode, args=[passwords])
    elif type(passwords) == type(''):
        return execute(jscode, args=[[passwords]])
    else:
        return None
