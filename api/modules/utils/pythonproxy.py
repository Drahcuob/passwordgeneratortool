#!/usr/bin/env python3
# encoding: utf-8

import execnet


def call_python_version(Version, Code, ArgumentList):
    gw      = execnet.makegateway("popen//python=python%s" % Version)
    channel = gw.remote_exec(Code % ("channel.receive()", "channel.send"))
    channel.send(ArgumentList)
    return channel.receive()
