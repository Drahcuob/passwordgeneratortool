from modules.types.module_types import module_types
from modules.utils.nodeproxy import execute

name = 'Rst'
function_name = 'rst'
description = """
Rst: password meter used here http://rumkin.com/tools/password/passchk.php.
"""
module_type = module_types.password_strength_meter

def rst(passwords):
    jscode = '''function (args) {
    var rst = require('./modules/rst/passchk.js');
    var scores = [];
    for (i = 0; i < args.length; i++) {
    scores.push({'password': args[i], 'result': rst(args[i])});
    }
    return scores;
    }
    '''

    if type(passwords) == type([]):
        return execute(jscode, args=[passwords])
    elif type(passwords) == type(''):
        return execute(jscode, args=[[passwords]])
    else:
        return None
        
