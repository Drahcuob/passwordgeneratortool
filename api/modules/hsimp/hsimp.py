from modules.types.module_types import module_types
from modules.utils.pythonproxy import call_python_version

name = 'Hsimp'
function_name = 'hsimp'
description = """
Hsimp: password meter used by https://howsecureismypassword.net/
"""
module_type = module_types.password_strength_meter

def hsimp(passwords):
    py2code = """
    import pwquality
    func = pwquality.PWQSettings().check
    args = %s
    results = []
    for arg in args:
        try:
            tmp = func(arg)
        except pwquality.PWQError as e:
            res = e[0] if e[0] >= 0 else 0
            results.append({ 'password': arg,
                             'result': res })
        else:
            res = tmp if tmp >= 0 else 0
            results.append({ 'password': arg,
                             'result': res })
    %s(results)
    """
    # print('A', passwords)
    if type(passwords) == type([]):
        return call_python_version("2.7", py2code, [ _.encode() for _ in  passwords ])
    elif type(passwords) == type(''):
        return call_python_version("2.7", py2code, [passwords])
    else:
        return None
