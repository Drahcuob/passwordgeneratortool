from modules.types.module_types import module_types
import itertools
import enchant
import string

name = 'Comprehensive8'
function_name = 'comprehensive8'
description = """
Comprehensive8: password must be longer than 8 characters, must have at least one uppercase letter, lowercase letter, symbol, and digit, and must not contain any dictionary words.
Predicted entropy: 30 bits.
Source: https://willcom217.wordpress.com/2015/02/09/p/
"""
module_type = module_types.password_rule

hascharfromcharset = lambda string, charset: all(1 for _ in string if _ in charset)
substringfromstring = lambda string: list(itertools.chain.from_iterable([string[idx:idx+size] for idx in range(len(string) - size + 1)] for size in range(4, len(string) + 1)))

"""
comprehensive8: password must be longer than 8 characters, must have at least one uppercase letter, lowercase letter, symbol, and digit, and must not contain any dictionary words.
"""
def compute(password):
    d = enchant.Dict("en_GB")
    v = int(len(password) > 8 and
               hascharfromcharset(password, string.ascii_lowercase) and
               hascharfromcharset(password, string.ascii_uppercase) and
               hascharfromcharset(password, string.digits) and
               hascharfromcharset(password, string.punctuation) and
               not any(map(d.check, substringfromstring(password))))
    return v * 100


def comprehensive8(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result': compute(password) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                   'result': compute(passwords) }
        ]
    else:
        return None
