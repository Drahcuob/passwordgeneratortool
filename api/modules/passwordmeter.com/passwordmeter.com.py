from modules.types.module_types import module_types
import collections
import string
import math

name = 'Passwordmeter.com'
function_name = 'chkPass'
description = """
Passwordmeter.com: password meter used by http://www.passwordmeter.com/
"""
module_type = module_types.password_strength_meter


# not sure why they're using this formula specifically
def calcUnqChar(passwd):
    # return = sum(e for e in collections.Counter(password.lower()) if e > 1)
    l = len(passwd)
    repChar = 0
    repInc = 0
    for a in range(l):
        charExist = False
        for b in range(l):
            if passwd[a] == passwd[b] and a != b:
                charExist = True
                repInc += abs(l / (b - a))
        if charExist:
            repChar += 1
            unqChar = l - repChar
            repInc = math.ceil(repInc / unqChar if unqChar else repInc)
    return [repChar, int(repInc)]

def calcConsec(passwd, charList):
    return sum(1 for i in range(1, len(passwd)) if passwd[i] in charList and passwd[i - 1] in charList)

def calcSeq(passwd, charList):
    return sum(1 for i in range(0, len(passwd) - 2) if passwd[i:i+3] in charList or passwd[i:i+3] in charList[::-1])

def calcOccur(passwd, charList):
    return sum(1 for c in passwd if c in charList)

def compute(password):
    minRequirement = 4
    minLength = 8
    # I don't know why they aren't using string.punctuation + string.whitespace
    puncWUnderscore = string.punctuation.replace('_', '')

    length = len(password)
    alphaUC = calcOccur(password, string.ascii_uppercase)
    alphaLC = calcOccur(password, string.ascii_lowercase)
    number = calcOccur(password, string.digits)
    symbol = calcOccur(password, puncWUnderscore)
    midChar = calcOccur(password[1:-1], puncWUnderscore + string.digits)
    requirements = (length >= minLength) + sum(not not v for v in [alphaUC, alphaLC, number, symbol])
    score = length * 4 + number * 4 + symbol * 6 + midChar * 2
    if alphaUC:
        score += (length - alphaUC) * 2
    if alphaLC:
        score += (length - alphaLC) * 2
    if requirements >= 4 and (length >= minLength):
        score += requirements * 2

    # print(length, alphaUC, alphaLC, number, symbol, midChar, requirements)

    alphasOnly = length if alphaUC + alphaLC == length else 0
    numbersOnly = length if number == length else 0
    unqChar = calcUnqChar(password.lower())
    consecAlphaUC = calcConsec(password, string.ascii_uppercase)
    consecAlphaLC = calcConsec(password, string.ascii_lowercase)
    consecNumber = calcConsec(password, string.digits)
    seqAlpha = calcSeq(password.lower(), string.ascii_lowercase)
    seqNumber = calcSeq(password, string.digits)
    # I don't know why they are using this order
    # seqSymbol = calcSeq(password, ")!@#$^&*()") # apparently this version doesn't give the same result as on the website, I should have missed something
    seqSymbol = calcSeq(password, "!@#$%^&*()") # this version does but I'm not sure why yet
    score -= alphasOnly + numbersOnly + unqChar[1] + consecAlphaUC * 2 + consecAlphaLC * 2 + consecNumber * 2 + seqAlpha * 3 + seqNumber * 3 + seqSymbol * 3

    # print(alphasOnly, numbersOnly, unqChar, consecAlphaUC, consecAlphaLC, consecNumber, seqAlpha, seqNumber, seqSymbol)

    return score if score >= 0 else 0

def chkPass(passwords):
    if type(passwords) == type([]):
        return [ { 'password': password,
                   'result': compute(password) }
                 for password in passwords ]
    elif type(passwords) == type(''):
        return [ { 'password': passwords,
                 'result': compute(passwords) }
        ]
    else:
        return None

# debug
# def main():
#     for i in range(10):
#         print("%s: %f" % ('a' * i, compute('a' * i)))

if __name__ == '__main__':
    main()
