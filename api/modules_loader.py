#!/usr/bin/python3
from os import listdir
from os.path import isdir, isfile, join
from importlib.machinery import SourceFileLoader

def load_module(path='modules/'):
    directories = ['%s/' % d for d in listdir(path) if isdir(join(path, d))]
    for directory in directories:
        onlyfiles = [f for f in listdir(path + directory) if isfile(join(path + directory, f))]
        for _ in onlyfiles:
            if _[-3:] == '.py':
                yield SourceFileLoader(_, path + directory + _).load_module()
