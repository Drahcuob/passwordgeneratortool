#!/usr/bin/python3
from flask import Flask, Response, request
from flask_cors import CORS
from random import randint, choice
from json import dumps
from functools import reduce
import modules_loader
import itertools
import csv
from os import listdir
from multiprocessing import Pool
from functools import partial
import math
import configuration

"""
Simple password generator server prototype.
"""

import logging
import logging.handlers

rotate_log_hanlder = logging.handlers.RotatingFileHandler(configuration.logfile_name, maxBytes=configuration.logfile_max_byte, backupCount=configuration.logfile_backup_count, encoding='utf-8')
rotate_log_hanlder.setLevel(configuration.logfile_level)

console_log_handler = logging.StreamHandler()
console_log_handler.setLevel(configuration.logfile_level)

log_formatter = logging.Formatter(configuration.logfile_format)
rotate_log_hanlder.setFormatter(log_formatter)
console_log_handler.setFormatter(log_formatter)

logger = logging.getLogger(__name__)
logger.setLevel(configuration.logfile_level)
logger.addHandler(rotate_log_hanlder)
logger.addHandler(console_log_handler)


modules = [ module for module in modules_loader.load_module() if hasattr(module, 'name') and hasattr(module, 'function_name') and hasattr(module, 'description') and hasattr(module, 'module_type')]
funcs = {module.name: getattr(module, module.function_name) for module in modules}

MAX_PASSWORDS_RATE = configuration.max_password_rate

app = Flask(__name__)
CORS(app)

def get_or_default(data, key, default):
    if data == None:
        return default
    return data[key] if key in data else default

@app.route('/upload/wordlist', methods=['POST'])
def upload_wordlist():
    if 'filename' not in request.json or 'data' not in request.json:
        logger.error('upload_wordlist: Missing args')
        return 'ko', 402
    if request.json['filename'] == '':
        logger.error('upload_wordlist: Filename is empty')
        return 'ko: filename is empty', 402
    try:
        with open(configuration.wordlist_directory + request.json['filename'], 'w') as ofile:
            ofile.write(request.json['data'].replace('\r', ''))
        logger.info('upload_wordlist: Upload wordlist done (%s)', configuration.wordlist_directory + request.json['filename'])
    except:
        logger.error('upload_wordlist: Upload wordlist failed (%s)', configuration.wordlist_directory + request.json['filename'])
    return 'ok', 200

def readnlines(fd, n):
    '''
    Read n lines from buffer stop when no more lines.
    '''
    lines = []
    m = 0
    while True:
        line = next(fd, None)
        if line == None:
            yield lines
            break
        line = line.rstrip()
        lines.append(line)
        m += 1
        if m == n:
            m = 0
            yield lines
            lines = []

def splitcsv(line, separator=',', quote='"'):
    fields = []
    current_field = ''
    status = 'default'
    p = None
    for c in line:
        if c == separator and status == 'default':
            fields.append(current_field)
            current_field = ''
        elif c == quote and (p == None or p != '\\'):
            status = 'quoted' if status == 'default' else 'default'
        elif c == '\\':
            if p == '\\':
                current_field += c
        else:
            current_field += c
        p = c
    if current_field != '' and status == 'default':
        fields.append(current_field)
    return fields 

def getpwds(name, lines):
    pwds = []
    for line in lines:
        if name.endswith('txt'):
            pwds.append(line.rstrip())
        else:
            parsed_line = splitcsv(line)
            if len(parsed_line) > 0:
                pwds.append(parsed_line[0])
    return pwds

def getpwds_and_score(name, lines):
    pwds_and_score = {}
    if name.endswith('txt'):
        return pwds_and_score
    for line in lines:
        parsed_line = splitcsv(line)
        if len(parsed_line) > 0:
            try:
                pwds_and_score[parsed_line[0]] = float(parsed_line[1])
            except:
                pass
    return pwds_and_score

def shannon_entropy(string):
    def entropy(string):
        prob = [ float(string.count(c)) / len(string) for c in dict.fromkeys(list(string)) ]
        entropy = - sum([ p * math.log(p) / math.log(2.0) for p in prob ])
        return entropy
    def max_entropy(string):
        if len(string) <= 0:
            return 0
        return math.log(len(string)) / math.log(2.0)
    entropyScore = 0 if max_entropy(string) == 0 else entropy(string) / max_entropy(string) * 100
    return entropyScore
    
def general_func(pwds, name=None):
    if name == None:
        return []
    return funcs[name](pwds)

@app.route('/tests/automated', methods=['POST'])
def tests_automated_handler():
    return tests_wordlist_handler()

@app.route('/tests/wordlist', methods=['POST'])
def tests_wordlist_handler():
    json = request.json
    if all([_ in json for _ in ['wordlist', 'modules', 'benchmark-method']]) == False:
        logger.error('tests_wordlist_handler: Wrong args')
        return "Wrong Args", 401
    limit = None
    if 'limit' in json:
        limit = int(json['limit'])
    wordlists = json['wordlist']
    benchmark_method = json['benchmark-method']
    modules_list = [module['name'] for module in json['modules']]
    if benchmark_method != 'default' and benchmark_method != 'file' and benchmark_method != 'none' and any([name == benchmark_method for name in modules_list]) is False:
        modules_list.append(benchmark_method)
    benchmark_data = {}
    def generate(modules_list, wordlists):
        yield '['
        with Pool(4) as p:
            for id_wordlist,wordlist in enumerate(wordlists):
                if id_wordlist != 0:
                    yield ', '
                title = wordlist
                if limit:
                    title += ' - limit: ' + str(limit)
                yield '{' + ' "title": "{0}", "data": '.format(title)
                with open(configuration.wordlist_directory + wordlist, 'r') as fd_wordlist:
                    yield '['
                    for key, name in enumerate(modules_list):
                        fd_wordlist.seek(0, 0)
                        pwd_counter = 0
                        module = {
                            'name': name,
                            'results': []
                        }
                        if key != 0:
                            yield ', '
                        func = partial(general_func, name=name)
                        while True:
                            raw_lines = []
                            dataset_size = limit if limit and limit < MAX_PASSWORDS_RATE*4 else MAX_PASSWORDS_RATE
                            for lines in readnlines(fd_wordlist, dataset_size):
                                raw_lines.append(lines)
                                if len(raw_lines) == 4:
                                    break
                            pwds = [getpwds(wordlist, elem) for elem in raw_lines]
                            if all([len(elem) == 0 for elem in raw_lines]):
                                break
                            if benchmark_method == 'file':
                                benchmark_data.update(getpwds_and_score(wordlist, lines))
                            results = p.map(func, pwds)
                            for result in results:
                                if result:
                                    module['results'].extend(result)
                                    pwd_counter += len(result)
                            if limit and pwd_counter >= limit:
                                module['results'] = module['results'][:limit]
                                break
                            logger.info('tests_wordlist_handler: %s %s tested', name, pwd_counter)
                        #if len(modules_list) != 1:
                        #    value_max = max(map(lambda x: x['result'], module['results']))
                        #    value_min = min(map(lambda x: x['result'], module['results']))
                        #    range_values = value_max - value_min
                        #    for idx in range(len(module['results'])):
                        #        tmp = module['results'][idx]
                        #        if range_values:
                        #            tmp['result'] = (tmp['result'] - value_min) / range_values
                        #        else:
                        #            tmp['result'] = 0
                        if module['name'] == benchmark_method:
                            module['name'] = 'Benchmark (' + module['name'] + ' PSM)'
                        yield dumps(module)
                    if len(modules_list) != 0 and (benchmark_method == 'file' or benchmark_method == 'default'):
                        if benchmark_method == 'file':
                            logger.info('tests_wordlist_handler: benchmark data from file')
                            module = {
                                'name': 'Benchmark (From file "' + wordlist + '")',
                                'results': [{
                                    'result': shannon_entropy(res['password']) if res['password'] not in benchmark_data else benchmark_data[res['password']],
                                    'password': res['password']
                                } for res in module['results']]
                            }
                            logger.info('tests_wordlist_handler: benchmark data from file done')
                        elif benchmark_method == 'default':
                            logger.info('tests_wordlist_handler: benchmark data (shannon entropy)')
                            module = {
                                'name': 'Benchmark (Default: Shannon Entropy)',
                                'results': [{
                                    'result': shannon_entropy(res['password']),
                                    'password': res['password']
                                } for res in module['results']]
                            }
                            logger.info('tests_wordlist_handler: benchmark data done (shannon entropy)')
                        yield ', '
                        yield dumps(module)
                    yield ']'
                yield '}'
            logger.info('tests_wordlist_handler: Tests done')
            yield ']'
    logger.info('tests_wordlist_handler: Launching test')
    return Response(generate(modules_list, wordlists), mimetype='application/json')

@app.route('/tests/imported', methods=['POST'])
def tests_imported_handler():
    rawcsv = request.data.decode('utf-8')
    reader = csv.reader(rawcsv.split('\n'), delimiter=',', quotechar='"')
    meters = []
    data = []
    current_wordlist = None
    current_meter = None
    for line in reader:
        if (line == []) or (line[0] == 'Wordlist' and line[1] == 'PSM' and line[2] == 'Password' and line[3] == 'Strength'):
            continue
        else:
            if current_wordlist == None or current_wordlist['title'] != line[0]:
                previous_title = line[0]
                if current_wordlist != None:
                    current_wordlist['data'] = meters
                    meters = []
                current_wordlist = {
                    'title': line[0],
                    'data': [] 
                }
                data.append(current_wordlist)
            if current_meter == None or current_meter['name'] != line[1]:
                current_meter = {
                    'name': line[1],
                    'results': []
                }
                current_meter['results'].append({
                    'password': line[2],
                    'result': float(line[3])
                })
                meters.append(current_meter)
            else:
                current_meter['results'].append({
                    'password': line[2],
                    'result': float(line[3])
                })
        if current_wordlist != None:
            current_wordlist['data'] = meters
    logger.info('tests_imported_handler: Import results')
    return Response(dumps(data), mimetype='application/json')

@app.route('/list/modules', methods=['GET'])
def list_modules_handler():
    logger.info('list_modules_handler: List modules')
    return Response(dumps([{'name': x.name, 'function_name': x.function_name, 'description': x.description, 'module_type' : x.module_type} for x in modules]), mimetype='application/json')

@app.route('/list/wordlists', methods=['GET'])
def list_wordlists_handler():
    logger.info('list_wordlists_handler: List modules')
    return Response(dumps(listdir(configuration.wordlist_directory)), mimetype='application/json')

if __name__ == '__main__':
    logger.info('main: Start server on %s:%s', configuration.host, configuration.port)
    app.run(host=configuration.host, port=configuration.port, use_reloader=False)
