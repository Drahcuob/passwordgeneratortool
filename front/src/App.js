import React, { Component } from 'react';
import Modal from 'react-modal';
import './App.css';

import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

import Table from './components/table';
import Barchart from './components/barchart';
import Linechart from './components/linechart';
import Differencechart from './components/differencechart.jsx';

const module_types = {
    password_strength_meter: 0,
    password_rule: 1
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meters: [],
      rules: [],
      wordlists: [],
      data: [],
      chartsdata: [],
      display_method: 'line',
      benchmark_method: 'default',
      limit_passwords: false,
	  display_progress_bar: false,
      modalWordlistUpload: false,
      modalFileImportCSV: false
    }

    this.modalStyle = {
		"content": {
			"top": '50%',
			"left": '50%',
			"right": 'auto',
			"bottom": 'auto',
			"width": '50%',
			"height": '30%',
			"marginRight": '-50%',
			"transform": 'translate(-50%, -50%)'
		} 
    }

    $.get('http://localhost:7890/list/modules', (data) => {
        var meters = [];
        var rules = [];
        data.forEach(function(elem) {
            if (elem.module_type === module_types.password_strength_meter)
            meters.push({
                name: elem.name,
                function_name: elem.function_name,
                description: elem.description,
                selected: false
            });
            else if (elem.module_type === module_types.password_rule)
            rules.push({
                name: elem.name,
                function_name: elem.function_name,
                description: elem.description,
                selected: false
            });
	});
        this.setState({ meters: meters, rules: rules });
    });

    $.get('http://localhost:7890/list/wordlists', (data) => {
      this.setState({ wordlists: data });
    });
    this.fileInputRef = React.createRef();
    this.wordlistInputRef = React.createRef();
    this.wordlistRef = React.createRef();
    this.displayMethodRef = React.createRef();
    this.uploadedWordlistNameRef = React.createRef();
    this.selectAllButtonRef = React.createRef();
    this.selectRuleAllButtonRef = React.createRef();
    this.limitNumberOfPwdRef = React.createRef();
  }

  select_all() {
      var meters = this.state.meters.slice();
      var next_state = this.selectAllButtonRef.current.checked;
      meters.forEach(function(meter) {
          meter.selected = next_state;
      });
      this.setState({ meters: meters });
  } 

  select_all_rule() {
      var rules = this.state.rules.slice();
      var next_state = this.selectRuleAllButtonRef.current.checked;
      rules.forEach(function(rules) {
          rules.selected = next_state;
      });
      this.setState({ rules: rules });
  } 

  change_state(meter_name) {
      var meters = this.state.meters.slice();
      var elem = meters.find(elem => elem.name === meter_name);
      var rules = this.state.rules.slice();
      if (elem === undefined)
	  elem = rules.find(elem => elem.name === meter_name);
      elem.selected = !elem.selected;
      this.setState({ meters: meters, rules: rules });
  }

  change_benchmark_method(new_method) {
    this.setState({ benchmark_method: new_method });
  }

  automated_test() {
      var modules = this.state.meters.concat(this.state.rules).filter((elem) => {
	  return elem.selected
      });
      var data = [];
      modules.forEach(elem => {
	  data.push({ name: elem.name, function_name: elem.function_name });
      })
      var benchmark_method = this.state.benchmark_method;
      var jsondata = {
          'modules': data ,
          'wordlist': [ 'length.txt', 'alphabet-8.txt', 'alphabet-12.txt', 'french.txt', 'british-english.txt', 'geometrical.txt' ],
          'benchmark-method': benchmark_method
      }
      this.setState({ display_progress_bar: true })
      $.ajax({
	  timeout: 0,
	  method: 'POST',
	  url: 'http://localhost:7890/tests/automated',
	  contentType: 'application/json',
	  data: JSON.stringify(jsondata),
	  success: data => {
	      var new_state = {
		  data: data,
		  chartsdata: this.state.chartsdata.concat(data[0]),
		  display_progress_bar: false	
	      }
              this.setState(new_state);
	  },
      });
  }

  update_data() {
      var modules = this.state.meters.concat(this.state.rules).filter((elem) => {
	    return elem.selected
      });
      var data = [];
      modules.forEach(elem => {
	    data.push({ name: elem.name, function_name: elem.function_name });
      })
      
      var wordlists = [];
      this.wordlistRef.current.childNodes.forEach(elem => {
        if (elem.selected) {
            wordlists.push(elem.value);
        }
      });

      var benchmark_method = this.state.benchmark_method;
      var jsondata = {
          'modules': data ,
          'wordlist': wordlists,
          'benchmark-method': benchmark_method
      }
      if (this.state.limit_passwords) {
          jsondata['limit'] = this.limitNumberOfPwdRef.current.value;
      }
      this.setState({ display_progress_bar: true })
      $.ajax({
          timeout: 0,
          method: 'POST',
          url: 'http://localhost:7890/tests/wordlist',
          contentType: 'application/json',
          data: JSON.stringify(jsondata),
          success: data => {
              var new_state = {
                  data: data,
                  chartsdata: this.state.chartsdata.concat(data[0]),
                  display_progress_bar: false	
              }
              this.setState(new_state);
          },
      });
  }

  export_data(data) {
    var content = [`"Wordlist","PSM","Password","Strength",\n`];
    data.forEach(wordlist => {
        wordlist.data.forEach(meter => {
            meter.results.forEach(function(result) {
                content.push('"' + wordlist.title + '","' + meter.name + '","' + result.password + '","' + result.result + '",\n');
            });
        });
    });
    var file = new Blob(content, {type: 'text/csv'})
    // Download on IE10+ and Others.
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, 'PSM-output.csv');
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = 'PSM-output.csv';
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
  }
  
  import_wordlist() {
    const fileInput = this.wordlistInputRef.current;
    const fileName = this.uploadedWordlistNameRef.current;

    if (fileInput.files.length >= 0) {
        const file = fileInput.files[0]

        var reader = new FileReader();
        reader.readAsText(file, 'utf-8');
        reader.onload = evt => {
            $.ajax({
                timeout: 0,
                type: 'POST',
                url: 'http://localhost:7890/upload/wordlist',
                contentType: 'application/json',
                data: JSON.stringify({
                    'data': evt.target.result,
                    'filename': fileName.value
                })
            })
        }
    }
  }

  import_data() {
    const fileInput = this.fileInputRef.current;

    if (fileInput.files.length >= 0) {
        const file = fileInput.files[0]

        var reader = new FileReader();
        reader.readAsText(file, 'utf-8');
        reader.onload = evt => {
            $.ajax({
              timeout: 0,
              type: 'POST',
              url: 'http://localhost:7890/tests/imported',
              contentType: 'text/csv',
              data: evt.target.result,
              success: data => {
                this.setState({ data: data })
                this.setState({ chartsdata: this.state.chartsdata.concat(data[0]) })
              }
            })
        }
    }
  }
  
  change_display_method() {
    var method = this.displayMethodRef.current.value;
    this.setState({display_method: method})
  }

  display_data(method) {
    if (method === 'line') {
        return <React.Fragment>
            {
                this.state.data.map((elem, key) => (
                    <Linechart title={ elem.title } key={ "linechart" + key.toString() } chartid={ "linechart" + key.toString() } data={ elem.data }></Linechart>
                ))
            }
        </React.Fragment>
    } else if (method === 'difference') {
        return <React.Fragment>
            {
                this.state.data.map((elem, key) => (
                    <Differencechart title={ elem.title } key={ "differencechart" + key.toString() } chartid={ "differencechart" + key.toString() } data={ elem.data }></Differencechart>
                ))
            }
        </React.Fragment>
    } else if (method === 'bar') {
        return <React.Fragment>
            {
                this.state.data.map((elem, key) => (
                    <Barchart title={ elem.title } chartid={ "barchart" + key.toString() } key={ "barchart" + key.toString() } data={ elem.data }></Barchart>
                ))
            }
        </React.Fragment>
    } else if (method === 'wordlistbar') {
        function is_in(array, elem) {
            for (var i=0; i<array.length; i++) {
                if (array[i].title === elem) {
                    return true;
                }
            }
            return false;
        }
        function get_elem_by_title(array, title) {
            for (var i=0; i<array.length; i++) {
                if (array[i].title === title) {
                    return array[i];
                }
            }
            return undefined;
        }
        var tmp_data = [];
        this.state.data.forEach(wordlist_results => {
            var name = wordlist_results.title;
            wordlist_results.data.forEach(results => {
                if (!is_in(tmp_data, results.name)) {
                    tmp_data.push({
                        'title': results.name,
                        'data': []
                    });    
                }
                var elem = get_elem_by_title(tmp_data, results.name);
                if (elem) {
                    elem['data'].push({
                        'name': name,
                        'results': results.results
                    })
                }
            });
        });
        return <React.Fragment>
            {
                tmp_data.map((elem, key) => (
                    <Barchart title={ elem.title } chartid={ "barchart" + key.toString() } key={ "barchart" + key.toString() } data={ elem.data }></Barchart>
                ))
            }
        </React.Fragment>
    } else if (method === 'table') {
        return <React.Fragment>
            {
                this.state.data.map((elem, key) => (
                    <Table limit="25" page="1" data={ elem.data }></Table>
                ))
            }
        </React.Fragment>
    }
  }

  validateModalFileImportCSV() {
    const fileField = this.fileInputRef.current.value;
    if (fileField === "" || fileField === undefined) {
        return false;
    }
    return true
  }

  validateModalWordlistUpload() {
    const fileField = this.wordlistInputRef.current.value;
    const nameField = this.uploadedWordlistNameRef.current.value;

    if (fileField === "" || fileField === undefined) {
        return false;
    }
    if (nameField === "" || fileField === undefined) {
        return false;
    }
    return true;
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href>Password Meter Tester</a>
			
			<div className="collapse navbar-collapse">
                <ul className="navbar-nav mr-auto">
					<li className="nav-item dropdown">
						<a className="nav-link dropdown-toggle" href id="wordlistDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">File</a>
						<div className="dropdown-menu" aria-labelledby="wordlistDropdown">
							<a className="dropdown-item" href onClick={() => this.export_data(this.state.data) }>Export as CSV</a>
							<a className="dropdown-item" href onClick={() => this.setState({ modalFileImportCSV: true }) }>Import from CSV</a>
						</div>
					</li>
					<li className="nav-item dropdown">
						<a className="nav-link dropdown-toggle" href id="wordlistDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wordlists</a>
						<div className="dropdown-menu" aria-labelledby="wordlistDropdown">
							<a className="dropdown-item" href onClick={() => this.setState({ modalWordlistUpload: true }) }>Upload</a>
						</div>
					</li>
                </ul>
			</div>
        </nav>

        <Modal
            isOpen={ this.state.modalFileImportCSV }
            onRequestClose={ () => this.setState({ modalFileImportCSV: false }) }
            style={ this.modalStyle }
            contentLabel="File import from CSV"
        >
            <h2 onClick={() => this.setState({ modalFileImportCSV: false }) }>Import from file</h2>

            <div className="container">
                <div className="row">
                    <div className="input-group mb-3">
                        <div className="custom-file">
                            <label className="custom-file-label" htmlFor="inputGroupFile01">Choose file</label>
                            <input ref={ this.fileInputRef } type="file" className="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="input-group-prepend" style={{ width: '100%' }}>
                        <button
                            className="input-group-text btn btn-success"
                            style={{ width: '100%' }}
                            id="inputGroupFileAddon01"
                            onClick={() => {
                                if (this.validateModalFileImportCSV()) {
                                    this.import_data()
                                    this.setState({modalFileImportCSV: false})
                                }
                            }}
                        >Import CSV data</button>
                    </div>
                </div>
            </div>
        
        </Modal>

        <Modal
            isOpen={ this.state.modalWordlistUpload }
            onRequestClose={ () => this.setState({ modalWordlistUpload: false }) }
			style={ this.modalStyle }
            contentLabel="Wordlist upload"
        >
            
            <h2 onClick={() => this.setState({ modalWordlistUpload: false }) }>Wordlist upload</h2>

			<div className="container">
			<div className="row">
				<input type="text" className="form-control" ref={ this.uploadedWordlistNameRef } placeholder="Wordlist name" />
			</div>
			<div className="row">
				<div className="input-group mb-3">
					<div className="custom-file">
						<label className="custom-file-label" htmlFor="inputGroupFile02">Choose file</label>
						<input ref={ this.wordlistInputRef } type="file" className="custom-file-input" id="inputGroupFile02" aria-describedby="inputGroupFileAddon02" />
					</div>
				</div>
			</div>
			<div className="row">
				<div className="input-group-prepend" style={{ width: '100%' }}>
					<button 
                        className="input-group-text btn btn-success text-center"
                        style={{ width: '100%' }}
                        id="inputGroupFileAddon01"
                        onClick={() => {
                            if (this.validateModalWordlistUpload()) {
                                this.import_wordlist()
                                this.setState({ modalWordlistUpload: false })
                            }
                        }}
                    >Upload wordlist</button>
				</div>
			</div>
			</div>
			
            
        </Modal>

        <br/><br/>

        <ul className="list-group">
            <li className='list-group-item' key='header'>
                <span className='list-right-align'>Choose benchmark method</span>
            </li>
            <li className='list-group-item' key='none' >
                <span>No Benchmark Data</span>
                <input
                    type='checkbox'
                    className="list-right-align"
                    readOnly
                    onClick={() => this.change_benchmark_method('none') }
                    checked={ this.state.benchmark_method === 'none' }
                />
            </li>
            <li className='list-group-item' key='default' >
                <span>Default</span>
                <input
                    type='checkbox'
                    className="list-right-align"
                    readOnly
                    onClick={() => this.change_benchmark_method('default') }
                    checked={ this.state.benchmark_method === 'default' }
                />
            </li>
            <li className='list-group-item' key='file' >
                From file
                <input
                    type='checkbox'
                    className="list-right-align"
                    readOnly
                    onClick={() => this.change_benchmark_method('file') }
                    checked={ this.state.benchmark_method === 'file' }
                />
            </li>
            <div className='list-group-item' key='rules' >
	        Password Rules:
            <li className='list-group-item' key='rule-all'>
                <input
                    type='checkbox'
                    readOnly
                    onClick={() => this.select_all_rule() }
                    ref={ this.selectRuleAllButtonRef }
                /> All
            </li>
          {
            this.state.rules.map((rule) => (
                <li className='list-group-item' key={ rule.name }>
                    <input onClick={() => this.change_state(rule.name) } type='checkbox' readOnly checked={ rule.selected }/>
                    <span onClick={() => this.change_state(rule.name) }> { rule.name } 
                        { rule.selected === true && <span className='list-item-description'> { rule.description } </span> }
                    </span>
                    <input
                        type='checkbox'
                        className="list-right-align"
                        readOnly
                        onClick={() => this.change_benchmark_method(rule.name) }
                        checked={ this.state.benchmark_method === rule.name }
                    />
                </li>
            ))
          }
            </div>
            <div className='list-group-item' key='meters' >
	        Password Strength Meters:
            <li className='list-group-item' key='meter-all'>
                <input
                    type='checkbox'
                    readOnly
                    onClick={() => this.select_all() }
                    ref={ this.selectAllButtonRef }
                /> All
            </li>
          {
            this.state.meters.map((meter) => (
                <li className='list-group-item' key={ meter.name }>
                    <input onClick={() => this.change_state(meter.name) } type='checkbox' readOnly checked={ meter.selected }/>
                    <span onClick={() => this.change_state(meter.name) }> { meter.name } 
                        { meter.selected === true && <span className='list-item-description'> { meter.description } </span> }
                    </span>
                    <input
                        type='checkbox'
                        className="list-right-align"
                        readOnly
                        onClick={() => this.change_benchmark_method(meter.name) }
                        checked={ this.state.benchmark_method === meter.name }
                    />
                </li>
            ))
          }
            </div>
        </ul>
        <br/>


        <div className="container-fluid">
            <div className="row">
                <div className="col">
                    <select multiple className="custom-select" ref={ this.wordlistRef }>
                      {
                        this.state.wordlists.map((wordlist) => (
			                 <option key={ wordlist } value={ wordlist }>{ wordlist }</option>
                        ))
                      }
                    </select>
                </div>

                <div className="col">
                    <button className="btn btn-info form-control" onClick={() => this.update_data()}>Test with wordlist !</button>
                </div>
            </div>

            <div className="row">
                <div className="col">
                    <button className={ "btn form-control col " + (!this.state.limit_passwords ? 'btn-success' : 'btn-danger' ) } onClick={() => this.setState({ limit_passwords: !this.state.limit_passwords })}>{ !this.state.limit_passwords ? "No password limit" : "Password limit:" } </button>
                </div>
                <div className="col">
                    <input type="number" className="form-control col" disabled={ !this.state.limit_passwords } ref={ this.limitNumberOfPwdRef } />
                </div>
            </div>

            <br/>

            <div className="row">
                <div className="col">
                    <button className="btn btn-info form-control col" onClick={() => this.automated_test() }>Launch Automated Test</button>
                </div>
            </div>
            
            <hr/><br/>

            <div className="row">
                <div className="col">
                    <select className="custom-select" defaultValue="line" onChange={() => this.change_display_method()} ref={ this.displayMethodRef }>
			            <option value="line">Line chart</option>
			            <option value="difference">Difference chart</option>
			            <option value="bar">Bar chart</option>
			            <option value="wordlistbar">Bar chart by Wordlist</option>
			            <option value="table">Results table</option>
                    </select>
                </div>
            </div>
        </div>

		<div className="progress" style={ {display: this.state.display_progress_bar ? 'block' : 'none' } }>
			<div className="progress-bar progress-bar-striped progress-bar-animated w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				Computing passwords... Please wait
			</div>
		</div>

        <br /><br />

        <div>
        {
            this.display_data(this.state.display_method)
        }
        </div>
      </div>
    );
  }
}


export default App;
