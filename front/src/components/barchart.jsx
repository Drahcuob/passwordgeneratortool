import React, { Component } from 'react';

import * as d3 from 'd3';

// https://blog.risingstack.com/d3-js-tutorial-bar-charts-with-javascript/

class Barchart extends Component {

    constructor(props) {
      super(props);
      this.data = [];
    }

    parseData() {
      if (this.props.data) {
        var data = []
        this.props.data.forEach(meter => {
          var rawdata = {
            0.25: 0,
            0.50: 0,
            0.75: 0,
            1.00: 0
          };
          meter.results.forEach(elem => {
            if (elem.result <= 25) {
                rawdata[0.25] += 1
            } else if (elem.result <= 50) {
                rawdata[0.50] += 1
            } else if (elem.result <= 75) {
                rawdata[0.75] += 1
            } else {
                rawdata[1] += 1
            }
          })

          var previous = 0;
          Object.keys(rawdata).forEach(key => {
            for (var i = previous + 1; i < key; i++) {
              if (rawdata[i] === undefined) {
                rawdata[i] = 0;
              }
            }
            previous = key;
          });

          var rawdata_array = [];
          Object.keys(rawdata).forEach(key => {
            rawdata_array.push({
              "value": key,
              "sum": rawdata[key]
            })
          });
          data.push({
            "name": meter.name,
            "data": rawdata_array
          })
        });
        this.data = data;
      }
    }

    computeXDomain(data) {
      // This function find values for all meters and sort them.
      var domains = [0.25, 0.5, 0.75, 1];
      return domains;
    }

    computeYDomain(data) {
        var domains = [0];
        var maxvalue = 0;

        Object.keys(data).forEach(key => {
          var buf_maxvalue = d3.max(data[key].data, elem => elem.sum);

          if (buf_maxvalue > maxvalue) {
            maxvalue = buf_maxvalue;
          }
        })
        domains.push(maxvalue);
        return domains;
    }

    drawChart() {
      const data = this.data;

      if (data.length !== 0) {
        d3.select('#' + this.props.chartid + ' > *').remove();

        var svg = d3.select('#' + this.props.chartid);


        const margin = 80;
        const width = window.innerWidth - 2 * margin;
        const height = 600 - 2 * margin;
    
        var legend_labels = data.map(elem => {
            return {
                name: elem.name,
                color: elem.color
            }
        });

        svg.attr('width', window.innerWidth).attr('height', `${600 + (20 * legend_labels.length)}px`);
        svg = svg.append('g').attr('transform', `translate(${margin}, ${margin})`);

        const xDomain = this.computeXDomain(data);
        const yDomain = this.computeYDomain(data);

        const xScale = d3.scaleBand().domain(xDomain).range([0, width])
        const yScale = d3.scaleLinear().domain(yDomain).range([height, 0])
        
        const xAxis = svg.append('g');
        
        xAxis.append('line')
         .attr('id', 'xaxis')
         .attr('x1', 0)
         .attr('y1', yScale(0))
         .attr('x2', width)
         .attr('y2', yScale(0))
         .attr('stroke', '#000');

        const xAxisLegends = xAxis.append('g');
        const legendOffset = xScale(0.5) / 2; 
        const legends = ['Very Weak', 'Weak', 'Strong', 'Very Strong']
        

        xAxisLegends.selectAll('text').data(xDomain).enter()
         .append('text')
         .attr('class', 'xaxislegends')
         .attr('x', d => xScale(d) + legendOffset)
         .attr('y', yScale(0) + 20)
         .attr('text-anchor', 'middle')
         .text((d, i) => legends[i])

        var labels = ["arect", "brect", "crect", "drect", "erect", "frect", "grect", "hrect", "irect", "jrect", "krect"]
        var nb_meters = Object.keys(data).length;
        var barwidth = (xScale.bandwidth() - 20) / nb_meters

        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');

        function getTextWidth(text, fontSize, font) {
            context.font = fontSize + 'px ' + font;
            return context.measureText(text).width;
        }

        Object.keys(data).forEach((key, idx) => {
          svg.selectAll(labels[idx]).data(data[key].data).enter().append("rect")
          .attr("x", d => xScale(d.value) + 10 + barwidth * idx)
          .attr("y", d => yScale(d.sum))
          .attr("width", barwidth)
          .attr("height", d => height - yScale(d.sum))
          .attr("fill", data[key].color)
          .on("mouseover", (d, i) => {
            svg.selectAll(".textoverlabel" + i).remove();
            svg.append("text")
              .attr("class", "textoverlabel" + i)
              .attr("font-size", "14px")
              .attr("x", (x, i) => {
                  let xcoord = xScale(d.value) + 10 + barwidth * idx;
                  let textWidth = getTextWidth(data[key].name, '14');
                  let offset = xcoord + textWidth;
                  if (offset > width) {
                    xcoord -= offset - width;
                  }
                  return xcoord;
              })
              .attr("y", yScale(d.sum) - 15)
              .text(data[key].name + ": " + d.sum);
          })
          .on("mouseout", (d, i) => {
            svg.selectAll(".textoverlabel" + i).remove();
          });
        })

        var legend = svg.selectAll('.legend').data(legend_labels).enter().append('g');

        legend.append('rect').attr('width', 10).attr('height', 10)
            .attr('x', 30)
            .attr('y', (d, i) => height + 50 + (20 * i))
            .style('fill', (d, i) => d.color)
            .style('stroke', (d, i) => d.color)

        svg.append("g")
            .call(d3.axisLeft(yScale))
            .append("text")
            .attr("fill", "#000")
            .attr("x", 40)
            .attr("y", -20)
            .attr("font-size", "1.5em")
            .attr("text-anchor", "start")
            .text(this.props.title);

        legend.append('text').attr('x', 50).attr('y', (d, i) => height + 60 + (20 * i)).text(d => d.name)
      }
    }

    databind(data) {
        var c_scale = d3.scaleSequential().domain([0, data.length]).interpolator(d3.interpolateViridis);

        data.forEach((d, i) => {
            if (d.name.startsWith('Benchmark')) {
                d.color = "#FF0033";
            } else {
                d.color = c_scale(i);
            }
        })
    }

    componentDidMount() {
      this.parseData();
      this.databind(this.data);
      this.drawChart();
    }

    componentDidUpdate() {
      this.parseData();
      this.databind(this.data);
      this.drawChart();
    }

    render() {
      return (
        <svg id={ this.props.chartid } />
      );
    }
}
  
export default Barchart;
