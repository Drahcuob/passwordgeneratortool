import React, { Component } from 'react';

import * as d3 from 'd3';

class Distributionchart extends Component {
    constructor(props) {
        super(props);
        this.data = []
    }

    parseData() {
        if (this.props.data) {
            var data = this.props.data;
            var parsed_data = [];
            Object.keys(data).forEach(k => {
                var parsed_elem = {
                    name: data[k].name,
                    values: []
                }
                data[k].results.forEach((v, i) => {
                    parsed_elem.values.push({
                        x: i,
                        y: v.result / 100
                    })
                })
                parsed_data.push(parsed_elem);
            });
            parsed_data.forEach(meter => {
                meter.values.sort((a, b) => a.y - b.y)
                meter.values.forEach((a, i) => a.x = i)
            });
            this.data = parsed_data;
        }
    }

    getXDomains(data) {
        var domains = [0];
        var x = 0;
        data.forEach(elem => {
            elem.values.forEach(item => {
                if (parseInt(item.x) > parseInt(x)) {
                    x = item.x;
                }
            })
        })
        domains.push(parseInt(x));
        return domains;
    }

    getYDomains(data) {
        var domains = [0];
        var y = 0;
        data.forEach(elem => {
            elem.values.forEach(item => {
                if (parseInt(item.y) > parseInt(y)) {
                    y = item.y;
                }
            })
        })
        domains.push(parseInt(y));
        return domains;
    }


    drawChart() {
        d3.select('#' + this.props.chartid + ' > *').remove();

        var data = this.data;

        if (data.length > 0) {
            var svgWidth = window.innerWidth, svgHeight = 400;

            var margin = { top: 20, right: 20, bottom: 30, left: 50 };
            var width = svgWidth - margin.left - margin.right;
            var height = svgHeight - margin.top - margin.bottom;

            var legend_labels = data.map(elem => elem.name);

            var svg = d3.select("#" + this.props.chartid).attr("width", window.innerWidth).attr("height", 500 + (20 * legend_labels.length));

            var g = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
            
            var x = d3.scaleLinear().rangeRound([0, width]);
            var y = d3.scaleLinear().rangeRound([height, 0]);

            var line = d3.line()
                .x(function(d) { return x(d.x)})
                .y(function(d) { return y(d.y)})

            x.domain(this.getXDomains(data))
            y.domain(this.getYDomains(data))

            g.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x))
                .select(".domain")
                .remove();

            g.append("g")
                .call(d3.axisLeft(y))
                .append("text")
                .attr("fill", "#000")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", "0.71em")
                .attr("text-anchor", "end")
                .text("Sum");

            g.append("g")
                .call(d3.axisLeft(y))
                .append("text")
                .attr("fill", "#000")
                .attr("x", 40)
                .attr("y", 10)
                .attr("font-size", "1.5em")
                .attr("text-anchor", "start")
                .text(this.props.title);

            var lines = g.selectAll(".line")
                .data(data)
                .enter().append("g")
                .attr("class", "line");

            var colors = ["#17A2B8", "#A2B817", "#327681", "#B817A2", "#813276", "#768132"];

            lines.append("path")
                .attr("class", "line")
                .attr("fill", "none")
                .attr("stroke", (d, i) => {
                    if (d.name.startsWith('Benchmark')) {
                        return "#FF0033";
                    }
                    return colors[i]
                })
                .attr("d", function(d) {
                    return line(d.values);
                })

            var legend = g.selectAll('.legend').data(legend_labels).enter().append('g');

            legend.append('rect').attr('width', 10).attr('height', 10)
                .attr('x', 30)
                .attr('y', (d, i) => height + 50 + (20 * i))
                .style('fill', (d, i) => {
                    if (d.startsWith('Benchmark')) {
                        return "#FF0033";
                    }
                    return colors[i]
                })
                .style('stroke', (d, i) => {
                    if (d.startsWith('Benchmark')) {
                        return "#FF0033";
                    }
                    return colors[i]
                })

            legend.append('text').attr('x', 50).attr('y', (d, i) => height + 60 + (20 * i)).text(d => d)
        }
    }

    componentDidMount() {
        this.parseData();
        this.drawChart();
    }
  
      componentDidUpdate() {
        this.parseData();
        this.drawChart();
    }
  
      render() {
        return (
          <svg id={ this.props.chartid } />
        );
    }
}

export default Distributionchart;
