import React, { Component } from 'react';

import * as d3 from 'd3';

class Differencechart extends Component {
    constructor(props) {
        super(props);
        this.data = []
    }

    parseData() {
        if (this.props.data) {
            var data = this.props.data;
            var parsed_data = [];
            Object.keys(data).forEach(k => {
                var parsed_elem = {
                    name: data[k].name,
                    values: []
                }
                data[k].results.forEach((v, i) => {
                    parsed_elem.values.push({
                        x: i,
                        y: v.result / 100 > 1 ? 1.0 : v.result / 100,
                        password: v.password,
                        score: v.result / 100
                    })
                })
                parsed_data.push(parsed_elem);
            });
            var benchmark = null;
            parsed_data.forEach((meter, i, o) => {
                meter.values.forEach((a, i) => a.x = i)
                if (meter.name.startsWith("Benchmark"))
                {
                    benchmark = meter;
                    o.splice(i, 1);
                }
            });
            parsed_data.forEach(meter => {
                let error_rate = 0
                meter.values.forEach((a, i) => {
                    a.y -= benchmark.values[i].y;
                    if (a.y <= 0)
                        error_rate += 1
                });
                error_rate = (error_rate * 100) / meter.values.length;
                meter.name += " Error Rate: " + error_rate.toString() + "%";
            })
            this.data = parsed_data;
        }
    }

    databind(data) {
        var c_scale = d3.scaleSequential().domain([0, data.length]).interpolator(d3.interpolateViridis);

        data.forEach((d, i) => {
            if (d.name.startsWith('Benchmark')) {
                d.color = "#FF0033";
            } else {
                d.color = c_scale(i);
            }
        })
    }

    getXDomains(data) {
        var domains = [0];
        var x = 0;
        data.forEach(elem => {
            elem.values.forEach(item => {
                if (parseInt(item.x) > parseInt(x)) {
                    x = item.x;
                }
            })
        })
        domains.push(parseInt(x));
        return domains;
    }

    getYDomains(data) {
        return [-1, 1];
    }

    drawChart() {
        d3.select('#' + this.props.chartid + ' > *').remove();

        var data = this.data;

        if (data.length > 0) {
            var svgWidth = window.innerWidth
            var svgHeight = 400;

            var margin = { top: 20, right: 20, bottom: 30, left: 50 };
            var width = svgWidth - margin.left - margin.right;
            var height = svgHeight - margin.top - margin.bottom;

            var legend_labels = data.map(elem => {
                return {
                    name: elem.name,
                    color: elem.color
                }
            });

            var svg = d3.select("#" + this.props.chartid).attr("width", window.innerWidth).attr("height", 500 + (20 * legend_labels.length));

            var g = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
            
            var x = d3.scaleLinear().rangeRound([0, width]);
            var y = d3.scaleLinear().rangeRound([height, 0]);

            var line = d3.line()
                .x(function(d) { return x(d.x)})
                .y(function(d) { return y(d.y)})

            var xDomains = this.getXDomains(data)
            var yDomains = this.getYDomains(data)

            x.domain(xDomains)
            y.domain(yDomains)

            g.append("g")
                .attr("transform", "translate(0," + y(0) + ")")
                .call(d3.axisBottom(x))
                .select(".domain")
                .append("line")
                .attr("x1", x(xDomains[0]))
                .attr("y1", y(0))
                .attr("x2", x(xDomains[1]))
                .attr("y2", y(0))
                .attr("stroke", "#000")
                .remove();

            g.append("g")
                .call(d3.axisLeft(y))
                .append("text")
                .attr("fill", "#000")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", "0.71em")
                .attr("text-anchor", "end")
                .text("Sum");

            g.append("g")
                .call(d3.axisLeft(y))
                .append("text")
                .attr("fill", "#000")
                .attr("x", 40)
                .attr("y", 10)
                .attr("font-size", "1.5em")
                .attr("text-anchor", "start")
                .text(this.props.title);

            var lines = g.selectAll(".line")
                .data(data)
                .enter().append("g")
                .attr("class", "line");

            lines.append("path")
                .attr("class", "line")
                .attr("fill", "none")
                .attr("stroke", (d, i) => {
                    return d.color
                })
                .attr("d", function(d) {
                    return line(d.values);
                })

                var symbol = function(i) {
                    console.log(i);
                    var symbols = [d3.symbolCircle, d3.symbolCross, d3.symbolDiamond, d3.symbolSquare, d3.symbolStar, d3.symbolTriangle, d3.symbolWye]
			        return d3.symbol().size(40).type(symbols[i % symbols.length]);
                }
            
                data.forEach((data, i) => {
                    g.selectAll(".point.dataset-" + i)
						.data(data.values)
						.enter().append("path")
						.attr("class", "point dataset-" + i)
						.attr("fill", data.color)
						.attr("stroke", data.color)
						.attr("d", symbol(i))
						.attr("transform", function(d) {
							return "translate(" + x(d.x) + "," + y(d.y) + ")";
						})
                        .on("mouseover", (d, i) => {
                            svg.selectAll(".textoverlabel" + i).remove();
                            svg.append("text")
                              .attr("class", "textoverlabel" + i)
                              .attr("x", x(d.x) + 30)
                              .attr("y", y(d.y) - 15)
                              .text(d.password + ": " + (Math.round(d.score * 100) / 100));
                        })
                        .on("mouseout", (d, i) => {
                            svg.selectAll(".textoverlabel" + i).remove();
                        });
                });

				var legend = g.selectAll('.legend').data(legend_labels).enter().append('g');

				legend.append('line')
					.attr('x1', 25)
					.attr('y1', (d, i) => height + 50 + (20 * i) + 3)
                    .attr('x2', 45)
                    .attr('y2', (d, i) => height + 50 + (20 * i) + 3)
					.style('fill', (d, i) => d.color)
					.style('stroke', (d, i) => d.color)

                legend.append("path")
                    .attr("class", "point dataset")
                    .attr("fill", (d, i) => d.color)
                    .attr("stroke", (d, i) => d.color)
                    .attr("d", (d, i) => symbol(i)())
                    .attr("transform", function(d, i) {
                        return "translate(" + 35 + "," + (height + 50 + (20 * i) + 3) + ")";
                    })

				legend.append('text').attr('x', 50).attr('y', (d, i) => height + 60 + (20 * i)).text(d => d.name)
        }
    }

    componentDidMount() {
        this.parseData();
        this.databind(this.data);
        this.drawChart();
    }
  
    componentDidUpdate() {
        this.parseData();
        this.databind(this.data);
        this.drawChart();
    }
  
    render() {
        return (
            <svg id={ this.props.chartid } />
        );
    }
}

export default Differencechart;
