import React, { Component } from 'react';

import * as d3 from 'd3';

// https://blog.risingstack.com/d3-js-tutorial-bar-charts-with-javascript/

class WordlistBarchart extends Component {

    constructor(props) {
      super(props);
      this.data = [];
    }

    parseData() {
      if (this.props.data) {
        var data = []
        this.props.data.forEach(meter => {
          var rawdata = {
            0.25: 0,
            0.50: 0,
            0.75: 0,
            1.00: 0
          };
          meter.results.forEach(elem => {
            if (elem.result <= 25) {
                rawdata[0.25] += 1
            } else if (elem.result <= 50) {
                rawdata[0.50] += 1
            } else if (elem.result <= 75) {
                rawdata[0.75] += 1
            } else {
                rawdata[1] += 1
            }
          })

          var previous = 0;
          Object.keys(rawdata).forEach(key => {
            for (var i = previous + 1; i < key; i++) {
              if (rawdata[i] === undefined) {
                rawdata[i] = 0;
              }
            }
            previous = key;
          });

          var rawdata_array = [];
          Object.keys(rawdata).forEach(key => {
            rawdata_array.push({
              "value": key,
              "sum": rawdata[key]
            })
          });
          data.push({
            "name": meter.name,
            "data": rawdata_array
          })
        });
        this.data = data;
      }
    }

    computeXDomain(data) {
      // This function find values for all meters and sort them.
      var domains = [0.25, 0.5, 0.75, 1];
      return domains;
    }

    computeYDomain(data) {
        var domains = [0];
        var maxvalue = 0;

        Object.keys(data).forEach(key => {
          var buf_maxvalue = d3.max(data[key].data, elem => elem.sum);

          if (buf_maxvalue > maxvalue) {
            maxvalue = buf_maxvalue;
          }
        })
        domains.push(maxvalue);
        return domains;
    }

    drawChart() {
      const data = this.data;

      if (data.length !== 0) {
        d3.select('#' + this.props.chartid + ' > *').remove();

        var svg = d3.select('#' + this.props.chartid);


        const margin = 80;
        const width = window.innerWidth - 2 * margin;
        const height = 600 - 2 * margin;
    
        var legend_labels = data.map(elem => elem.name);

        svg.attr('width', window.innerWidth).attr('height', `${600 + (20 * legend_labels.length)}px`);
        svg = svg.append('g').attr('transform', `translate(${margin}, ${margin})`);

        const xScale = d3.scaleBand().domain(this.computeXDomain(data)).range([0, width])
        const yScale = d3.scaleLinear().domain(this.computeYDomain(data)).range([height, 0])
          
        svg.append('g').attr('transform', `translate(0, ${height})`).call(d3.axisBottom(xScale));
    
        svg.append('g').call(d3.axisLeft(yScale));

        var labels = ["arect", "brect", "crect", "drect", "erect", "frect", "grect", "hrect", "irect", "jrect", "krect"]
        var colors = ["#17A2B8", "#A2B817", "#327681", "#B817A2", "#813276", "#768132"]
        var nb_meters = Object.keys(data).length;
        Object.keys(data).forEach((key, idx) => {
          svg.selectAll(labels[idx]).data(data[key].data).enter().append("rect")
          .attr("x", d => xScale(d.value) + 10 + (((xScale.bandwidth() - 20) / nb_meters) * idx))
          .attr("y", d => yScale(d.sum))
          .attr("width", (xScale.bandwidth() - 20) / nb_meters)
          .attr("height", d => height - yScale(d.sum))
          .attr("fill", d => {
            if (data[key].name.startsWith('Benchmark')) {
                return "#FF0033";
            }
            return colors[idx]
          }) // 17a2b8
        })

        var legend = svg.selectAll('.legend').data(legend_labels).enter().append('g');

        legend.append('rect').attr('width', 10).attr('height', 10)
            .attr('x', 30)
            .attr('y', (d, i) => height + 50 + (20 * i))
            .style('fill', (d, i) => {
                if (d.startsWith('Benchmark')) {
                    return "#FF0033";
                }
                return colors[i]
            })
            .style('stroke', (d, i) => {
                if (d.startsWith('Benchmark')) {
                    return "#FF0033";
                }
                return colors[i]
            })

        svg.append("g")
            .call(d3.axisLeft(yScale))
            .append("text")
            .attr("fill", "#000")
            .attr("x", 40)
            .attr("y", -20)
            .attr("font-size", "1.5em")
            .attr("text-anchor", "start")
            .text(this.props.title);

        legend.append('text').attr('x', 50).attr('y', (d, i) => height + 60 + (20 * i)).text(d => d)
      }
    }

    componentDidMount() {
      this.parseData();
      this.drawChart();
    }

    componentDidUpdate() {
      this.parseData();
      this.drawChart();
    }

    render() {
      return (
        <svg id={ this.props.chartid } />
      );
    }
}
  
export default WordlistBarchart;
