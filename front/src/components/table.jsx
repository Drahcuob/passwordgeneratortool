import React, { Component } from 'react';

class Table extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
            page: parseInt(this.props.page, 10),
            limit: parseInt(this.props.limit, 10),
            sortByName: false,
            sortByPassword: false,
            sortByLevel: false,
            sortByStrength: false
        }
        this.mergedData = undefined
        this.limitRef = React.createRef();
    }

    sortData() {
        if (this.state.sortByName) {
            this.mergedData.sort((a, b) => {
                return ('' + a.name).localeCompare(b.name);
            });
        } else if (this.state.sortByPassword) {
            this.mergedData.sort((a, b) => {
                return ('' + a.password).localeCompare(b.password);
            });
        } else if (this.state.sortByLevel) {
            this.mergedData.sort((a, b) => {
                return ('' + a.level).localeCompare(b.level);
            });
        } else if (this.state.sortByStrength) {
            this.mergedData.sort((a, b) => {
                return b.result - a.result;
            });
        }
    }

    computeLevel(strength) {
        if (strength <= 0.25)
            return "Very Weak"
        if (strength <= 0.50)
            return "Weak"
        if (strength <= 0.75)
            return "Strong"
        return "Very Strong"
    }

    mergeData() {
        if (this.mergedData === undefined) {
            let mergedData = [];
            this.props.data.forEach(meter => {
                meter.results.forEach(result => {
                    let mergedResult = result;
                    mergedResult.name = meter.name;
                    mergedResult.level = this.computeLevel(result.result);
                    mergedData.push(mergedResult);
                })
            })
            this.mergedData = mergedData;
        }
    }

    componentWillMount() {
        this.mergeData();
        this.sortData();
    }

    componentWillUpdate() {
        this.mergeData();
        this.sortData();
    }
    
    display_data(meter)
    {
        var begin = (this.state.page-1) * this.state.limit;
        var end = begin + this.state.limit;
        var table = []

        for (; begin < end; begin++)
        {
            if (begin >= this.mergedData.length)
                return table
            const res = this.mergedData[begin]
            table.push(
                <tr>
                    <td>{ res.name }</td>
                    <td>{ res.password }</td>
                    <td>{ res.level }</td>
                    <td>{ res.result }</td>
                </tr>
            )
        }
        return table;
    }

    last_page() {
        let lpage = 0;
        
        if (this.mergedData) {
            lpage = Math.round(this.mergedData.length / this.state.limit)
        }
        return lpage;
    }

    is_first_page() {
        return this.state.page === 1;
    }

    is_last_page() {
        return this.state.page === this.last_page();
    }

    go_prev_page() {
        if (!this.is_first_page()) {
            var next_page = this.state.page - 1;
            this.setState({ page: next_page });
        }
    }

    go_next_page() {
        if (!this.is_last_page()) {
            var next_page = this.state.page + 1;
            this.setState({ page: next_page });
        }
    }

    onNameClick() {
        this.setState({
            sortByName: true,
            sortByPassword: false,
            sortByLevel: false,
            sortByStrength: false
        });
    }

    onPasswordClick() {
        this.setState({
            sortByName: false,
            sortByPassword: true,
            sortByLevel: false,
            sortByStrength: false
        });
    }

    onStrengthClick() {
        this.setState({
            sortByName: false,
            sortByPassword: false,
            sortByLevel: false,
            sortByStrength: true
        });
    }

    onLevelClick() {
        this.setState({
            sortByName: false,
            sortByPassword: false,
            sortByLevel: true,
            sortByStrength: false
        });
    }

    render() {
      if (this.props.data.length > 0) {
      return (
        <React.Fragment>
        <div className="container">
        <div className="row">
                <input
                    id="limit"
                    type="number"
                    className="form-control col-6"
                    defaultValue={ this.state.limit }
                    ref={ this.limitRef }
                />
                <button
                    className="btn btn-secondary col-6"
                    onClick={ () => this.setState({
                        limit: this.limitRef.current.value
                    }) }
                >Update limit!</button>
        </div>
        </div>
        <div className="row">
            <button className="btn btn-secondary col" onClick={() => this.go_prev_page() }>Previous</button>
            <span className="col-10 centered">{ this.state.page }/{ this.last_page() }</span>
            <button className="btn btn-secondary col" onClick={() => this.go_next_page() }>Next</button>
        </div>
        <table className="table">
            <thead>
                <tr>
                    <th onClick={ () => this.onNameClick() } >Name</th>
                    <th onClick={ () => this.onPasswordClick() } >Password</th>
                    <th onClick={ () => this.onLevelClick() } >Level</th>
                    <th onClick={ () => this.onStrengthClick() } >Strength</th>
                </tr>
            </thead>
            <tbody>
                {
                    this.display_data()
                }
            </tbody>
      </table> 
      </React.Fragment>
      );}

      return (<div></div>)
    }
}
  
export default Table;

  
